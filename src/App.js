import React, { Component, Fragment } from 'react'
import './css/App.css';
import './css/reset.css';
import Controls from './components/Controls';
import Timeline from './components/timeline/Timeline';
import Viewport from './components/viewport/Viewport';
import MenuBar from './components/MenuBar';

class App extends Component {
  constructor(props) {
    super(props)
    
    this.viewRef = React.createRef();
    this.timelineRef = React.createRef();

    this.programPrevTime = 0;

    this.state = {
      scale : 32,
      isPlaying : false,
      currentTime : 0,
      actives : [],
      newTimelineState : {},
    }

    this.timelineState = {
      animations : [],
      audios : [],
      selected : [],
    }

    //anchor
    this.anchor = [];
    this.isShift = false;

    //actives
    this.actives = [];
    this.prevTime = 0;
    this.idx = 0;
  
    //short-cut
    window.addEventListener('keydown', e=>{
      const {isPlaying} = this.state;

      const key = e.key;

      switch(key){
        case ' ':
          e.preventDefault();
          if(isPlaying){
              this.setState({
                isPlaying : false
              })
          }
          else{
              this.setState({
                isPlaying : true
              })
          }
          break;

        case 'End': //reset time
          e.preventDefault();
          this.resetTime();
          break;

        case 'ArrowLeft':
          e.preventDefault();
          this.setTime(this.state.currentTime-1);
          break;
        
        case 'ArrowRight':
          e.preventDefault();
          this.setTime(this.state.currentTime+1);
          break;

        default:
          break;
      }

      if(e.shiftKey)
        this.isShift = true;
    })

    window.addEventListener('keyup', e=>{
      const key = e.key;
      if(key==='Shift')
          this.isShift = false;
  })

  }//constructor

  componentDidMount(){
    window.requestAnimationFrame(this.animate);
  }

  //animate
  animate = time => { // program time
    let {isPlaying,currentTime} = this.state;
    
    if(isPlaying)
      this.setState({
        currentTime : currentTime + (time-this.programPrevTime)/1000,
      })
    //console.log(time, this.programPrevTime);
    this.programPrevTime = time;

    window.requestAnimationFrame(this.animate);
  }

  componentDidUpdate(prevProps, prevState){
    // TimeChanged
    if(prevState.currentTime !== this.state.currentTime){
      this.update();
    }
  }

  //Actives
  update = () => {
    const time = this.state.currentTime;
    const {animations,audios} = this.timelineState;
    const blocks = animations.concat(audios).sort(this.blockSort);

    if(time < this.prevTime)
      this.reset();
    //add actives
    while(blocks[this.idx]){
      const block = blocks[this.idx];
      if(block.start > time) break;
      if(block.end > time){
        this.actives.push(block);
      }
      this.idx++;
    }

    //remove active
    let i = 0;
    while(this.actives[i]){
      const block = this.actives[i];
      if(block.end<time){
        this.actives.splice(i,1);
        continue;
      }
      i++;
    }

    this.prevTime = time;

    this.effect(); //this.actives 수정
    this.setState({
      actives : this.actives.sort(this.activeSort),
    })
  }

  // effect 값 변경 -> timeline 변화 -> 무한 render
  effect = () =>{
    //actives(marker), currentTime, handleChangeTimelineBlock
    const {actives} = this;
    const {currentTime} = this.state;
    const newActives = actives.map(active => {

      const offset = currentTime - active.start;
      //console.log(active);
      const {marker} = active;
      console.log(marker);
      if(marker.length < 1) { return active };

      // 1. marker offset -> 0.5, 1.5, 2.0 --> 4개 구획

      //first
      let newEffect = {...marker[0].effect};
      //aniamtion
      for(let i=0; i<marker.length-1; i++){
        if(marker[i].offset<=offset && offset<=marker[i+1].offset){
            // 2. offset에 따라 effect값을 계산 
            const start = marker[i].offset;
            const end = marker[i+1].offset;
            const animationOffset = offset-start;
            const ratio = animationOffset / (end-start);

            //test
            const startEffect = marker[i].effect;
            const endEffect = marker[i+1].effect;

            //변화 값
            const cX = endEffect.x - startEffect.x;
            const cY = endEffect.y - startEffect.y;
            const cRotation = endEffect.rotation - startEffect.rotation;
            const cScale = endEffect.scale - startEffect.scale;
            const cOpacity = endEffect.opacity - startEffect.opacity;

            newEffect = {
              ...newEffect,
              x:startEffect.x + cX*ratio,
              y:startEffect.y + cY*ratio,
              rotation:startEffect.rotation + cRotation*ratio,
              scale:startEffect.scale + cScale*ratio,
              opacity:startEffect.opacity + cOpacity*ratio,
            }
        }
      }

      //last
      if(offset>marker[marker.length-1].offset){
        newEffect = {...marker[marker.length-1].effect};
      }
      return {
        ...active,
        effect:newEffect,
      };

    });

    this.actives = newActives;
  }

  reset = () =>{
    this.idx = 0;
    this.actives = [];
  }

   //blockSort 
   blockSort = ( a, b ) => { return a.start === b.start ? this.layerSort( a, b ) : a.start - b.start; } // sameStart -> layerSort
   layerSort = ( a, b ) => { return a.layer - b.layer; };
 
   //Activessort
   activeSort = ( a, b ) => { return a.layer === b.layer ? this.zIndexSort(a,b) : a.layer - b.layer }; //same layer -> zIndex Sort 
   zIndexSort = ( a, b ) => { return b.start - a.start; }; // Ascending Sort

  //set
  play = () => {
    this.setState({
      isPlaying : true,
    })
  }

  pause = () => {
    this.setState({
      isPlaying : false,
    })
  }

  setTime = time =>{
    this.setState({
      currentTime : Math.max(time,0),
      isPlaying : false,
    })
  }

  resetTime = () => {
    this.setState({
      currentTime : 0,
      isPlaying : false,
    })
  }

  
  handleChangeTimelineState = state => {
    this.setState({
      newTimelineState : state,
    }) 
  }

  handleChangeTimelineBlock = block => {
    const {timelineState} = this;
    const {animations,audios,selected} = timelineState;

    const data = {
      animations : animations.map(
          animation => animation.id === block.id
          ? {...block}
          : {...animation}
      ),
      audios : audios.map(
          audio => audio.id === block.id
          ? {...block}
          : {...audio}
      ),
      selected : selected,
    };

    this.handleChangeTimelineState(data);
  }

  handleMoverMouseDown = e =>{
    const {scale} = this.state;
    this.setTime(e.nativeEvent.offsetX/scale);
    const onMouseMove = e =>{
      if(e.target.className!=='time-mover') return;
      
      const{currentTime} = this.state;
      const {anchor} = this;

      let snapAnc = null;
      let lower = 0.5;

      for(const anc of anchor){
        const diff = Math.abs(anc-currentTime);
        if(diff<0.5)
          if(diff<lower){
            lower = diff;
            snapAnc = anc;
          }
      }
      
      if(this.isShift&&snapAnc!==null)
        this.setTime(snapAnc)
      else
        this.setTime(e.offsetX/scale);
    }

    const onMouseUp = () =>{

      document.removeEventListener("mousemove",onMouseMove);
      document.removeEventListener("mouseup",onMouseUp);
    }
    document.addEventListener("mousemove",onMouseMove);
    document.addEventListener("mouseup",onMouseUp)
  }

  setTimelineState = data => {
    this.timelineState = data;
    const {animations,audios} = this.timelineState;
    const blocks = animations.concat(audios);

    while(this.anchor.length)
      this.anchor.pop();

    for(const block of blocks){
      this.anchor.push(block.start);
      this.anchor.push(block.end);
      console.log(this.anchor);
    }
  }

  fullScreen = () =>{
    this.viewRef.current.requestFullscreen();
  }

  undo = () =>{
    this.timelineRef.current.undo();
  }

  redo = () =>{
    console.log(this.timelineRef);
    this.timelineRef.current.redo();
  }

  changeSaveFlag = flag => {
    this.timelineRef.current.changeSaveFlag(flag);
  }

  render() {
    const {fullScreen, handleMoverMouseDown, handleChangeTimelineState, handleChangeTimelineBlock, setTimelineState, changeSaveFlag} = this;
    const {isPlaying, currentTime, actives, scale, newTimelineState} = this.state;

    const offset = currentTime*scale;

    return (
      <Fragment>
      <header className="header">
        <div className='logo'>Logo</div>
      </header>
      <main className="main">
        <div className='visual-container'>
          <div className='media'>
            <section className="viewport-wrapper" ref={this.viewRef}>
              <Viewport
                actives = {actives}
                timelineState = {this.timelineState}
                time = {currentTime}
                onChange = {handleChangeTimelineState}
                onChangeBlock = {handleChangeTimelineBlock}
                changeSaveFlag = {changeSaveFlag}
              />
            </section> 
            <section className="controls-wrapper">
              <Controls 
                currentTime = {currentTime}
                isPlaying = {isPlaying}
                play = {this.play}
                pause = {this.pause}
                setTime = {this.setTime}
              />
            </section>
          </div>
          <section className='effect-menu-wrapper'> 
            <MenuBar
              scale = {scale}
              timelineState = {this.timelineState}
              time = {currentTime}
              actives = {actives}
              onChange = {handleChangeTimelineState}
              onChangeBlock = {handleChangeTimelineBlock}
              changeSaveFlag = {changeSaveFlag}
            />  
          </section>
        </div>

       <section className='timeline-editor'>
          <section className='editor-wrapper'>
            <div className='button-container'>
            <button onClick={this.undo}>U</button>
            <button onClick={this.redo}>R</button>
            <button onClick={fullScreen}>F</button>
            </div>
              
              <div className='area-info'>
                <div>Video</div>
                <div>Audio</div>
              </div>

            <div className='scale-slider'></div>

          </section>

          <section className="timeline-wrapper">

            <div
              className='time-mover'
              onMouseDown={handleMoverMouseDown}
            />

            <div className='timemarker' style={{transform:`translateX(${offset}px)`}}/>

            <Timeline 
              ref = {this.timelineRef}
              newState = {newTimelineState}
              scale = {scale}
              time = {currentTime}

              set = {setTimelineState}
              updateActives = {this.update}
              resetActives = {this.reset}
            />

          </section>
      </section>
      </main>
    </Fragment>
    )
  }
}

export default App