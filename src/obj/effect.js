const DEFAULT_EFFECT = 
{
    x : 480,
    y : 270,
    scale : 1,
    rotation : 0,
    opacity : 1,
};

class Effect{
    constructor({x,y,scale,rotation,opacity} = DEFAULT_EFFECT){
        this.x = x;
        this.y = y;
        this.scale = scale;
        this.rotation = rotation;
        this.opacity = opacity;
    }
}

export default Effect;