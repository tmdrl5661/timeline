import React, { Component } from 'react'
import Effect from '../obj/effect';


export class MenuEffect extends Component {
    constructor(props) {
        super(props)
    
        //marker
        this.onMarker = false;
        this.selectedMarker = {};
        this.selectedMarker.effect = new Effect();

        this.haveMarker = false;

        //default select
        const {block} = this.props;
        let selected = 0;
        if(block.marker.length>0) //haveMarker
            selected = block.marker[0].id;
        
         this.state = {
             selectMarkerId : selected,
         }
    }

    /*componentWillMount(){
        const {block} = this.props;

        for(const marker of block.marker)
            if(marker.id === this.state.selectMarkerId)
                this.selectedMarker = {
                    ...marker,
                    effect : {...marker.effect},
                }
    }*/
    shouldComponentUpdate(nextProps,nextState){
        const {block, timelineState} = nextProps;
        const {selected} = timelineState;
        const {selectMarkerId} = nextState;

        //another selected
        if(this.props.timelineState.selected !== selected) 
            this.setState({
                selectMarkerId : block.marker.length > 0 ? block.marker[0].id : 0,
            })

        for(const marker of block.marker)
            if(marker.id === selectMarkerId)
                this.selectedMarker = {
                    ...marker,
                    effect : {...marker.effect},
                }

        return true;
    }
    componentDidUpdate(){
        this.haveMarker = this.props.block.marker.length > 0;
    }
    handleMouseDown = e => {
        const {block, onChangeBlock, changeSaveFlag} = this.props;
        const name = e.target.dataset.name;

        if(this.haveMarker) return;

        let offset = 100;
        let max = Infinity;
        let min = -Infinity;
        if(name==='scale') {max=20; min=-20; offset=300;};
        if(name==='opacity') {max=1; min=0;};

        let value = block.effect[name];

        const onMouseMove = e =>{
            value += e.movementX / offset;

            const newBlock = {
                ...block,
                effect : {
                    ...block.effect,
                    [name] : Math.max(Math.min(value,max),min),
                },
            }
            console.log(newBlock);
            changeSaveFlag(false);
            onChangeBlock(newBlock);
        }

        const onMouseUp = () =>{
            changeSaveFlag(true);
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    }

    //sort
    offsetSort = ( a, b ) => { return a.offset - b.offset; };

    //marker
    handleCreateMarker = () =>{
        if(!this.onMarker) return;

        const {block,time, onChangeBlock} = this.props;
        const offset = time-block.start;

        const newMarker = {
            offset : offset,
            effect : block.marker.length>0?{...block.marker[block.marker.length-1].effect}:{...block.effect},
        }

        //소수점 첫째자리로 중복체크
        for(const marker of [...block.marker])
            if(marker.offset.toFixed(1) === newMarker.offset.toFixed(1))
                return;

        let id,max = 0;
        for(const marker of [...block.marker])
            if(marker.id>max)
                max = marker.id;
        
        id = max + 1;
        newMarker.id = id;

        const data = {
            ...block,
            marker:block.marker
            .concat(newMarker)
            .sort(this.offsetSort),
        }
        
        onChangeBlock(data);

        //created marker select
        this.setState({
            selectMarkerId : newMarker.id,
        });

    }

    handleSelectMarker = e =>{
        const selectedId = parseInt(e.target.value);
        this.setState({
            selectMarkerId : selectedId,
        })
    }

    handleMarkerMouseDown = e => {
        const {scale, block, onChangeBlock, changeSaveFlag} = this.props;
        const length = block.end-block.start;
        let value = this.selectedMarker.offset;

        const onMouseMove = e =>{
            changeSaveFlag(false);
            value += e.movementX / scale;
            value = Math.max(Math.min(value,length),0);
            
            const newMarkers = block.marker.map(mark => mark.id === this.selectedMarker.id
                ? {...mark,offset:value}
                : {...mark}
            )

            //marker[selected].offset = value
            const newBlock = {
                ...block,
                marker : newMarkers
                .sort(this.offsetSort),
            }

            onChangeBlock(newBlock);
        }
        const onMouseUp = () =>{
            changeSaveFlag(true);
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    }

    handleMarkerEffectMouseDown = e =>{
        const {block, onChangeBlock, changeSaveFlag} = this.props;
        const name = e.target.dataset.name;
        let value = this.selectedMarker.effect[name];
        
        let offset = 100;
        let max = Infinity;
        let min = -Infinity;

        if(name==='x'||name==='y') offset = 3;
        if(name==='scale') {max=20; min=-20; offset=300;};
        if(name==='opacity') {max=1; min=0;};

        const onMouseMove = e =>{
            changeSaveFlag(false);

            value += e.movementX / offset;

            const newMarkers = block.marker.map(mark => mark.id === this.selectedMarker.id
                ? {...mark,
                    effect:{ 
                        ...mark.effect, 
                        [name]:Math.max(Math.min(value,max),min), 
                    }
                }
                : {...mark}
            )

            const newBlock = {
                ...block,
                marker : newMarkers,
            }
        
            onChangeBlock(newBlock);
            
        }
        const onMouseUp = () =>{
            changeSaveFlag(true);
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    }

    handleMarkerRemove = () =>{
        const {block, onChangeBlock} = this.props;
        const id = this.state.selectMarkerId;

        const newMarkers = block.marker.filter( mark => mark.id !== id)

        const newBlock = {
            ...block,
            marker : newMarkers
        }
        
        onChangeBlock(newBlock);

        //select another marker
        if(newBlock.marker.length > 0){
            let anotherMark = newBlock.marker.find(mark => mark.id !== id);
            this.setState({
                selectMarkerId : anotherMark.id,
            })
        }
    }
  
    
    render() {
        const {handleMouseDown, handleMarkerMouseDown, handleMarkerEffectMouseDown, handleCreateMarker, handleSelectMarker, handleMarkerRemove} = this;
        const {time,actives,block} = this.props;

         this.onMarker = false;
         const length = block.end - block.start;
         const offset = time-block.start;
         if(0 <= offset && offset <= length) this.onMarker = true;
 
         let haveMarker = false;
         if(block.marker.length > 0)
             haveMarker = true;
 
         const options = block.marker.map( marker => 
             <option
                 key = {marker.id}
                 value = {marker.id}
             >{`Marker ${marker.id}`}</option>
         );

         let displayEffect = block.effect;
         console.log(block, block.effect);
         if(this.onMarker && haveMarker && actives.find(active => active.id === block.id) !== undefined){ // active in
            displayEffect = actives.find(active => active.id === block.id).effect;
         }

        return (
            <div className='menu'>
            <div className='block-info-wrapper'>
                <div>
                    <span className='header-text'>Scale :</span> 
                    <span className='edit-text' onMouseDown={handleMouseDown} data-name='scale'>{Math.round(displayEffect.scale*100)}%</span>
                </div>
                <div>
                    <span className='header-text'>Roitation :</span> 
                    <span className='edit-text' onMouseDown={handleMouseDown} data-name='rotation'>{Math.round(displayEffect.rotation*57.2958)}&deg;</span>
                </div>
                <div>
                    <span className='header-text'>Opacity :</span> 
                    <span className='edit-text' onMouseDown={handleMouseDown}  data-name='opacity'>{Math.round(displayEffect.opacity*100)}%</span>
                </div>

                <button className={`marker-button ${this.onMarker?'':'inactive'}`} onClick={handleCreateMarker}>Create Marker</button>
            </div>


            {/*markers*/}
            <div className={`marker-list-wrapper ${haveMarker?'':'d-none'}`}>
                <select onChange={handleSelectMarker} value={this.state.selectMarkerId}>
                    {options}
                </select>
                <div className='marker-effect-wrapper'>
                    <div>
                        <span className='header-text'>Offset :</span> 
                        <span className='edit-text' data-name='offset' onMouseDown={handleMarkerMouseDown}>{this.selectedMarker?.offset?.toFixed(1)}</span>
                    </div>
                    <div>
                        <span className='header-text'>Position :</span>
                        <div>
                            <span className='edit-text' data-name='x' onMouseDown={handleMarkerEffectMouseDown}>{Math.round(this.selectedMarker.effect.x)}</span>&nbsp;
                            <span className='edit-text' data-name='y' onMouseDown={handleMarkerEffectMouseDown}>{Math.round(this.selectedMarker.effect.y)}</span>&nbsp;
                        </div>
                    </div>
                    <div>
                        <span className='header-text'>Scale :</span> 
                        <span className='edit-text' data-name='scale' onMouseDown={handleMarkerEffectMouseDown}>{Math.round(this.selectedMarker.effect.scale*100)}%</span>
                    </div>
                    <div>
                        <span className='header-text'>Roitation :</span>
                        <span className='edit-text' data-name='rotation' onMouseDown={handleMarkerEffectMouseDown}>{Math.round(this.selectedMarker.effect.rotation*57.2958)}&deg;</span>
                    </div>
                    <div>
                        <span className='header-text'>Opacity :</span>
                        <span className='edit-text' data-name='opacity' onMouseDown={handleMarkerEffectMouseDown}>{Math.round(this.selectedMarker.effect.opacity*100)}%</span>
                    </div>
                </div>
                <div className='button-container'>
                    <button className='del-button' onClick={handleMarkerRemove}>X</button>
                </div>
            </div>
            {/*markers*/}


        </div>
        )
    }
}

export default MenuEffect