import React, { Component } from 'react'
import './css/Timeline.css';
import AnimationList from './Animation/AnimationList';

import Effect from '../../obj/effect';

class Timeline extends Component {
    constructor(props) {
        super(props)

        this.id = 0;

        this.state = {
             animations: [],
             audios : [],
             selected : [],
             saveFlag : true,
        }
        
        //move
        this.moveY = 0;

        //undo - redo
        this.undoStack = [{animations:this.state.animations, audios:this.state.audios}];
        this.redoStack = [];
        this.undoFlag = false;

        //anchor
        this.anchor = []; // currentTime, block start & end 
        this.anchor.push(this.props.time);
        this.isShift = false;

        //Short-cuts
        window.addEventListener('keydown', e=>{
            const key = e.key;
            if(e.ctrlKey){ // Ctrl + 
                switch(key){
                    case 'd': // duplicate
                        e.preventDefault();
                        this.remove();
                        break;
                    case 'c':
                        e.preventDefault();
                        this.duplicate();
                        break;
                    case 'a': // all select
                        e.preventDefault();
                        this.selectAll();
                        break;
                    case 'z': // undo
                        e.preventDefault();
                        this.undo();
                        break;
                    case 'y': // redo
                        e.preventDefault();
                        this.redo();
                        break;
                    default:
                        break;
                }
            }

            if(e.shiftKey)
                this.isShift = true;
        });

        window.addEventListener('keyup', e=>{
            const key = e.key;
            if(key==='Shift')
                this.isShift = false;
        })

    }//constructor

    shouldComponentUpdate(nextProps, nextState){
        const {newState, scale, time} = this.props;
        return newState !== nextProps.newState || scale !== nextProps.scale || time !== nextProps.time || this.state !== nextState;
    }

    componentDidUpdate(prevProps,prevState){
        const {newState} = this.props; // value
        const {set, updateActives, resetActives, time} = this.props; // function
        const {animations, audios, selected, saveFlag} = this.state;

        //newState
        if(prevProps.newState !== this.props.newState){
            this.id = this.getMaxId(newState)+1;
            this.setState({
                animations : newState.animations,
                audios : newState.audios,
                selected : newState.selected,
            })
        } 

        //Change State
        if(prevState !== this.state){
            set({
                animations : animations,
                audios : audios,
                selected : selected,
            });

            resetActives();
            updateActives();
        }

        //version
        if(!prevState.saveFlag && saveFlag){
           // console.log("tlfgod");
            this.versionSave();
        }
        if(this.state.animations !== prevState.animations || this.state.audios !== prevState.audios ){
            if(saveFlag){
              //  console.log("tlfgod");
                this.versionSave();
            }
        }
        //anchor
        if(time !== prevProps.time)
            this.anchor[0] = time;
    }

    layerSort = ( a, b ) => { return a.layer - b.layer; };
    startSort = ( a, b ) => { return a.start === b.start ? this.layerSort( a, b ) : a.start - b.start; } // sameStart -> layerSort

    //create
    handleCreate = (e,type) => {
        const {scale} = this.props;
        const {animations, audios} = this.state;
        const start = e.nativeEvent.offsetX / scale; 
        const end = start + 2.5;
        const layer = Math.floor( e.nativeEvent.offsetY / 48 );

        const effect = new Effect();

        if(type==='video')
            this.setState({
                animations : animations.concat({
                    id : this.id++,
                    type : 0,
                    name : 'Video',
                    start : start,
                    end : end,
                    layer :layer,
                    src : '',
                    effect : effect,
                    marker : [],
                })
                .sort(this.startSort)
            })
        else if(type === 'audio')
            this.setState({
                audios : audios.concat({
                    id : this.id++,
                    type:1,
                    name : 'Audio',
                    start : start,
                    end : end,
                    layer :layer,
                    src : '',
                    effect : effect,
                    marker : [],
                })
                .sort(this.startSort)
            })
    }

    //select
    selectOnly = id => {
        this.setState({
            selected : [id],
        })
    }

    selectToggle = id => {
        const {selected} = this.state;

        let newSelect = [];

        if(selected.includes(id))
            newSelect = selected.filter(select => select !== id);
        else
            newSelect = selected.concat(id);

        this.setState({
            selected :  newSelect,
        })
    }

    selectAll = () => {
        const {animations, audios} = this.state;
        const animationIds = animations.map( animation => animation.id );
        const audioIds = audios.map( audio => audio.id );

        this.setState({
            selected : animationIds.concat(audioIds),
        })
        
    }

    unSelectAll = e =>{
        e.stopPropagation();
        this.setState({
            selected : [],
        })
    }
    
    //Animation Modified
    handleUpdate = data => {
        const {animations, audios} = this.state;
        this.setState({
            animations : animations.map( 
                animation => animation.id === data.id
                ? {...animation, ...data}
                : {...animation}
            ).sort(this.startSort),
            audios : audios.map(
                audio => audio.id === data.id
            ? {...audio, ...data}
            : {...audio}
            ).sort(this.startSort)
        })

    }

    handleMoving = ( block, movement ) => {
        const { animations, audios, selected } = this.state;

        //move Y
        let offset = 0;
        this.moveY += movement.y;

        if(this.moveY >= 46){
            offset++;
            this.moveY = 0;
        }
        if(this.moveY <= -46){
            offset--;
            this.moveY = 0;
        }
       
        if(selected.includes(block.id)){ // Selecte All Move
            this.setState({
                animations : animations.map(
                    animation => selected.includes(animation.id)
                    ? { ...animation,
                        start : Math.max(animation.start+movement.x, 0),
                        end : Math.max(animation.end+movement.x, animation.end-animation.start),
                        layer : Math.max(animation.layer + offset, 0),
                    }
                    : animation
                ).sort(this.startSort),
                audios : audios.map(
                    audio => selected.includes(audio.id)
                    ? { ...audio,
                        start : Math.max(audio.start+movement.x, 0),
                        end : Math.max(audio.end+movement.x, audio.end-audio.start),
                        layer : Math.max(audio.layer + offset, 0),
                    }
                    : audio
                ).sort(this.startSort),
            });
        }
        else{ // Move Only
            const length = block.end - block.start;

            //snap
            const {anchor} = this;

            const movedData = {
                ...block,
                start : Math.max(block.start + movement.x ,0),
                end : Math.max(block.end + movement.x, length),
                layer : Math.max(block.layer + offset, 0),
            }

            let data = {...movedData};
            
            if(Math.abs(anchor[0]-block.start)<0.5){
                data = {
                    ...block,
                    start : anchor[0],
                    end : anchor[0]+length,
                }
            }
            else if(Math.abs(this.anchor[0]-block.end)<0.5){
                data = {
                    ...block,
                    start : anchor[0]-length,
                    end : anchor[0],
                }
            }

            if(this.isShift)
                this.handleUpdate(data);
            else
                this.handleUpdate(movedData);
        }

    }

    remove = () => {
        const { animations, audios, selected } = this.state;

        if(selected.length < 1 ) return;

        this.setState({
            animations : animations.filter( animation => 
                !selected.includes(animation.id)
            ),
            audios : audios.filter( audio => 
                !selected.includes(audio.id) 
            ),
            selected : [],
        })
    }

    duplicate = () =>{
        const {animations,audios, selected} = this.state;
        
        if(selected.length < 1) return;
        
        let dupAnimation = [];
        let dupAudio = [];
        let newSelect = [];

        for(const id of selected){
           const block = this.findBlock(id);

            const size = block.end - block.start;

            newSelect.push(this.id);

            /*
            let markerList = [];
            if(block.marker.length>0)
                markerList = [...block.marker]
            */

            const newMarker = block.marker.map( mark => {return{...mark,effect:{...mark.effect}}})

            const obj = {
                id : this.id++, 
                type : block.type,
                name : `${block.name} Copy`,
                start : block.start + size,
                end : block.end + size,
                layer : block.layer,
                src : block.src,
                effect : {...block.effect}, //obj
                marker : newMarker,
            }

            if(block.type === 0) // animation
                dupAnimation.push(obj);
            else if (block.type === 1) // audio
                dupAudio.push(obj);
        }

        this.setState({
            animations : animations.concat(dupAnimation),
            audios : audios.concat(dupAudio),
            selected : newSelect,
        })
       
    }

    changeSaveFlag = flag => {
        this.setState({
            saveFlag : flag,
        })
    }

    // Undo Redo
    undo = () =>{
        let {undoStack,redoStack} = this;

        if(undoStack.length < 2 ) return;

        const undoState = undoStack.pop();
       // console.log(undoStack);
        redoStack.push(undoState);
       // console.log(redoStack);
        const topState = undoStack[undoStack.length-1]; // peek

        this.undoFlag = true;

        this.setState({
            animations : topState.animations,
            audios : topState.audios,
            selected : [],
        })
       // console.log(undoStack);
        undoStack.pop();
    }

    redo = () => {
        let{undoStack, redoStack} = this;

        if(redoStack.length < 1) return;

        const redoState = redoStack.pop();
        undoStack.push(redoState);
        const topState = undoStack[undoStack.length-1]; // peek

        this.undoFlag = true;

        this.setState({
            animations : topState.animations,
            audios : topState.audios,
            selected : [],
        })
       // console.log(undoStack);
        undoStack.pop();

    }

    //fucntion
    findBlock = id =>{
        const {animations, audios} = this.state;

        const block = animations.concat(audios).find(animation => animation.id === id);

        return block;
    }

    getMaxId = data =>{
        const maxCallback = (max, cur) => Math.max(max,cur);

        const arr = data.animations.concat(data.audios);
        const arrId = arr.map( block => block.id );
        const maxId = arrId.reduce(maxCallback,0); // arrId.length < 1 : return 0

        return maxId;
    }

    versionSave = () => {
        const {animations,audios} = this.state;

        this.undoStack.push({animations:animations,audios:audios});
        
        if(this.undoFlag)
            this.undoFlag = false;
        else
            this.redoStack = [];
    }

    render() {
        const {handleCreate,handleUpdate, handleMoving, selectToggle, selectOnly, changeSaveFlag, unSelectAll} = this;
        const {scale} = this.props;
        const {animations, audios, selected} = this.state;

        return (
            <div className='timeline' onClick={unSelectAll}>
                <div className='wrapper'>
                    <div className='timeline-video' onDoubleClick={e=>{handleCreate(e,'video')}} >
                        <AnimationList
                            scale = {scale}
                            animations = {animations}
                            selected = {selected}

                            onUpdate = {handleUpdate}
                            onMoving = {handleMoving}
                            selectToggle = {selectToggle}
                            selectOnly = {selectOnly}
                            changeSaveFlag = {changeSaveFlag}
                        />
                    </div>
                </div>
                <div className='wrapper'>
                    <div className='timeline-audio' onDoubleClick={e=>{handleCreate(e,'audio')}}>
                        <AnimationList 
                            scale = {scale}
                            animations = {audios}
                            selected = {selected}

                            onUpdate = {handleUpdate}
                            selectToggle = {selectToggle}
                            selectOnly = {selectOnly}
                            onMoving = {handleMoving}
                            changeSaveFlag = {changeSaveFlag}
                        />
                    </div>
                </div>
                
            </div>
        )
    }
}

export default Timeline