import React, { Component } from 'react'
import '../css/Marker.css';

export class Marker extends Component {
    shouldComponentUpdate(nextProps){
        return this.props.marker !== nextProps.marker;
    }

    handleMouseDown = e =>{
        e.stopPropagation();
        const onMouseMove = e =>{

        }
        const onMouseUp = () =>{
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    }

    handleSelect = e =>{

    }

    render() {
        const {marker, scale} = this.props;
        const left = marker.offset*scale - 7.5;

        //const {handleMouseDown, handleSelect} = this;
        return (
            <div className='marker' 
                style={{transform:`translateX(${left}px)  rotate(45deg)`}}
                //onMouseDown= {handleMouseDown} 
            />
        )
    }
}

export default Marker
