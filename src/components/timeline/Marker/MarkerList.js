import React, { Component, Fragment } from 'react'
import Marker from './Marker';

export class MarkerList extends Component {
    shouldComponentUpdate(nextProps){
        return this.props.markerlist !== nextProps.markerlist;
    }
    render() {
        const {markerlist, scale} = this.props;
        const list = markerlist.map(
                        marker=><Marker
                                    key = {marker.id}
                                    marker = {marker}
                                    scale = {scale}
                                />
                    )

        return (
            <Fragment>
                {list}
            </Fragment>
        )
    }
}

export default MarkerList
