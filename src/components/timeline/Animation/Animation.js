import React, { Component } from 'react'
import '../css/Animation.css';

import MarkerList from '../Marker/MarkerList';

class Animation extends Component {
    constructor(props) {
        super(props)
        
        this.input = React.createRef();
    }

    shouldComponentUpdate(nextProps){
        return nextProps.animation !== this.props.animation || nextProps.selected !== this.props.selected;
    }

    handleDblClick = e =>{
        e.stopPropagation();

        const event = new MouseEvent('click',{
            'view': window,
            'bubbles': true,
        })

        this.input.current.dispatchEvent(event);
    }


    handleInput = e =>{
        const {animation,onUpdate} = this.props;

        const files = e.target.files;
        if(files.length < 1) return;

        const file = files[0];

        const reg = new RegExp("image/(jpg|jpeg|png)");
        if(file.type.match(reg)===null){
            alert("jpg/jpeg/png 타입");
            return;
        } 

        let reader = new FileReader();
        reader.readAsDataURL(file);
        
        let data = {...animation};
        reader.addEventListener("load",()=>{
            data.name = file.name
            data.src = reader.result;

            onUpdate(data);
        })

    }

    handleSelect = e =>{
        e.stopPropagation();
        const {animation, selectToggle, selectOnly} = this.props;

        if(e.ctrlKey) // animation: select toogle
            selectToggle(animation.id);
        
        else // animation : select only
           selectOnly(animation.id)

    }

    //moving
    handleBlockMouseDown = () => {
        const {changeSaveFlag} = this.props;
        const onMouseMove = e =>{
            const {animation,scale, onMoving} = this.props;
            let movement = { x:e.movementX/scale, y:e.movementY}

            changeSaveFlag(false);
            onMoving(animation,movement);
        }

        const onMouseUp = () =>{
            changeSaveFlag(true);
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }

        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);

    }

    //sizing
    handleResizeMouseDown = dire => {
        const {changeSaveFlag} = this.props;
        const onMouseMove = e =>{
            console.log("실행")
            const {animation,scale,onUpdate} = this.props;
            let data = {
                ...animation,
                start : animation.start,
                end : animation.end,
            };

            if(dire === 'l')
                data.start += e.movementX/scale;
            else if(dire === 'r')
                data.end += e.movementX/scale;

            changeSaveFlag(false);

            onUpdate(data);
        }
        const onMouseUp = () =>{
            changeSaveFlag(true);
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    }


    render() {
        const {animation, selected, scale} = this.props;
        const {start,end,layer} = animation;
        const size = (end - start) * scale;

        const inner = animation.name;

        return (
            <div className={`block ${animation.type===1?'audio':''} ${selected?'select':''} `}
                style = {{
                    width : size,
                    left : start * scale,
                    top : layer * 48
                }}
                onClick = {this.handleSelect}
                onDoubleClick={this.handleDblClick}
                onMouseDown = {this.handleBlockMouseDown}
            >
                {inner}
                <input 
                    type='file' 
                    ref={this.input}
                    onInput = {this.handleInput}
                />
                <div className='resize left' onMouseDown={e=>{ e.stopPropagation(); this.handleResizeMouseDown('l') }}  />
                <div className='resize right' onMouseDown={e=>{ e.stopPropagation(); this.handleResizeMouseDown('r') }} />
                <MarkerList 
                    scale = {scale}
                    markerlist = {animation.marker}
                />
            </div>
        )
    }
}

export default Animation
