import React, { Component } from 'react'
import Animation from './Animation';

class AnimationList extends Component {
     shouldComponentUpdate(nextProps){
        return nextProps.animations !== this.props.animations || nextProps.selected !== this.props.selected ;
     }

    render() {
        const {animations,scale, selected, onUpdate, onMoving, selectToggle, selectOnly, changeSaveFlag} = this.props;
        
        const list = animations.map(animation => {
            const select = selected.includes(animation.id);
            return(
                <Animation
                    key = {animation.id}
                    scale = {scale}
                    animation = {animation}
                    selected = {select}

                    onUpdate = {onUpdate}
                    onMoving = {onMoving}
                    selectToggle = {selectToggle}
                    selectOnly = {selectOnly}
                    changeSaveFlag = {changeSaveFlag}
                />
            )
        })

        return (
            <div className="animation-container">
                {list}
            </div>
        )
    }
}

export default AnimationList
