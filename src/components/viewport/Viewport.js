import React, { Component } from 'react'
import './css/Viewport.css'
import * as PIXI from 'pixi.js'

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST

class Viewport extends Component {
    constructor(props){
        super(props);
        this.cc = null;
        //this.stageRef = React.createRef();
        this.pixi_cnt = null;
        this.block = undefined;
        this.app = new PIXI.Application({width:960, height:540}); // center : 480, 270
        this.app.stage.sortableChildren = true;
    }

    updataPixiCnt = element => {
        this.pixi_cnt = element;
        this.pixi_cnt.appendChild(this.app.view);
    }
    handleclick = e =>{
        console.log("tlfgod");
    }
    onDragStart = e => {
        e.target.data = e.data;
        e.target.dragging = true;
        console.log(e.target);
    }
    onDragEnd = e => {
        const {onChangeBlock} = this.props;
        if(e.target instanceof PIXI.Sprite){
            e.target.dragging = false;
            e.target.data = null;
            const newBlock = {
                ...this.block,
                effect : {
                    ...this.block.effect,
                },
            }
            console.log(newBlock);
            onChangeBlock(newBlock);
        }
    }
    onDragMove = e => {
        if(e.target instanceof PIXI.Sprite){
            if (e.target.dragging) {
                const newPosition = e.target.data.getLocalPosition(e.target.parent);
                //selectTool.unselect();
                //this.count = 0;
                e.target.x = newPosition.x;
                e.target.y = newPosition.y;
                this.block.effect.x = e.target.x;
                this.block.effect.y = e.target.y;
                
            /*//  if(this.x > app.renderer.screen.width){
            //   this.x = app.renderer.screen.width;
            ///  }
            // if(this.y > app.renderer.screen.height){
                //  this.y = app.renderer.screen.height;
            // }
            // if(this.x < app.renderer.screen.x){
                // this.x = app.renderer.screen.x;
            // }
            // if(this.y < app.renderer.screen.y){
                //  this.y = app.renderer.screen.y
                //}*/
            }
        }
    } 
    render() {
       const {actives} = this.props;
       const {timelineState} = this.props;
       const {animations, audios, selected} = timelineState;
       this.block = selected.length === 1 ? animations.concat(audios).find(animations => selected[0] === animations.id) : undefined;
       const list = actives.filter(
           active => active.src !== ''
       ).map(
           active => {
                const obj = new PIXI.Sprite(PIXI.Texture.from(active.src))
                obj.anchor.set(0.5);
                obj.alpha = active.effect.opacity;
                obj.rotation = active.effect.rotation;
                obj.x = active.effect.x;
                obj.y = active.effect.y;
                obj.scale.set(active.effect.scale);
                obj.interactive = true;
                obj.buttonMode = true;
                obj
                  .on('click', this.handleclick)
                  .on('pointerdown', this.onDragStart)
                  .on('pointerup', this.onDragEnd)
                  .on('pointerupoutside', this.onDragEnd)
                  .on('pointermove', this.onDragMove);
                console.log(obj)
                return obj;
            }
       ); 

        if(list.length === 0){
            this.app.stage.removeChildren();
        }
        else{
            this.app.stage.removeChildren()
                for(let i = 0; i < list.length; i++){
                    this.app.stage.addChildAt(list[list.length-1-i],i);
                }
        }

        return (
            <div ref = {this.updataPixiCnt} />
        )
    }
}
export default Viewport 