import React, { Component } from 'react'
import Menu from './Menu';
import './css/MenuBar.css';

class MenuBar extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            tap : '0',
        }
    }

    handleTapChoise = e => {

        this.setState({
            tap : e.target.dataset.tap,
        })
    }
    
    
    render() {
        const {tap} = this.state;
        const {timelineState} = this.props;
        const {animations, audios, selected} = timelineState;
        if(selected.length === 1){
            var block = animations.concat(audios).find(animation => selected[0] === animation.id);
        }
        console.log(block);
        return (
            <div className='effect-menu'>
                <div className='tap-container'>
                    <div className={`${tap==='0'?'select':''}`} onClick={this.handleTapChoise} data-tap='0'>Project</div>
                    <div className={`${tap==='1'?'select':''}`} onClick={this.handleTapChoise} data-tap='1'>Block</div>
                    <div className={`${tap==='2'?'select':''}`} onClick={this.handleTapChoise} data-tap='2'>Effect</div>
                </div>
                <div className='menu-wrapper'>
                    <Menu
                        tap = {this.state.tap}
                        scale = {this.props.scale}
                        block = {block}
                        time = {this.props.time}
                        actives = {this.props.actives}

                        onChange = {this.props.onChange}
                        onChangeBlock = {this.props.onChangeBlock}
                        timelineState = {this.props.timelineState}
                        changeSaveFlag = {this.props.changeSaveFlag}
                    />
                </div>
            </div>
        )
    }
}

export default MenuBar
