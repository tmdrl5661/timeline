import React, { Component } from 'react'
import './css/Controls.css';

class Controls extends Component {
    render() {
        const {currentTime, isPlaying} = this.props;
        const minutes = Math.floor( currentTime / 60 );
		const seconds = currentTime % 60;
		const padding = seconds < 10 ? '0' : '';
        const timeText =`${minutes}:${padding}${seconds.toFixed(2)}`;
        
        return (
            <div className='controls'>
                <div className='time-display'>{timeText}</div>
                <div className='button-container'>
                    <button 
                        className='prev'
                        onClick={()=>{this.props.setTime(currentTime-1)}}
                    />
                    <button 
                        className={`play ${isPlaying?'pause':''}`}
                        onClick = {()=>{isPlaying?this.props.pause():this.props.play()}} 
                    />
                     <button 
                        className='next'
                        onClick={()=>{this.props.setTime(currentTime+1)}}
                    />
                </div>
            </div>
        )
    }
}

export default Controls
