import React, { Component } from 'react'
import './css/Menu.css';
import MenuEffect from './MenuEffect';


class Menu extends Component {
    constructor(props) {
        super(props)
        
        this.moveY = 0;
    }


    handleInput = e =>{
        const files = e.target.files;
        if(files.length < 1) return;

        const file = files[0];

        if(file.type !== 'text/plain') return;

        let reader = new FileReader();
        reader.readAsText(file);
        
        const {onChange} = this.props;

        reader.addEventListener("load",()=>{
            const importData = JSON.parse(reader.result);
            importData.selected = [];
            onChange(importData);
        })
        e.target.value = "";
    }

    export = () =>{
        const {timelineState} = this.props;
        const fileName = prompt("파일 명을 입력하세요");

        if(fileName === null) return;
        if(fileName.length<1) return;

        const exportData = {
            animations : timelineState.animations,
            audios : timelineState.audios,
        }

        this.saveToFile_Chrome( fileName, JSON.stringify(exportData) )
    }
    //saveToFile
    saveToFile_Chrome = (fileName, content) => {
        let blob = new Blob([content], { type: 'text/plain'});
        let objURL = window.URL.createObjectURL(blob);
                
        // 이전에 생성된 메모리 해제
        if (window.__Xr_objURL_forCreatingFile__) {
            window.URL.revokeObjectURL(window.__Xr_objURL_forCreatingFile__);
        }
        window.__Xr_objURL_forCreatingFile__ = objURL;
    
        let aTag = document.createElement('a');
        aTag.download = fileName;
        aTag.href = objURL;
        aTag.click();
    }

    handleMouseDown = e => {
        const {block, onChangeBlock, scale, changeSaveFlag} = this.props;
        const name = e.target.dataset.name;
        let value = block[name];
        const onMouseMove = e =>{
            value += e.movementX/scale ;

            const newBlock = {
                ...block, // steel id
                [name] : value, 
            }

            changeSaveFlag(false);
            onChangeBlock(newBlock);
        }
        const onMouseUp = () =>{
            changeSaveFlag(true);
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup',onMouseUp);
        }
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    }

    //function
    getTime = value =>{
        const minutes = Math.floor( value / 60 );
        const seconds = value % 60;
        const padding = seconds < 10 ? '0' : '';
        const timeText =`${minutes}:${padding}${seconds.toFixed(2)}`;

        return timeText;
    }

    readMe = () => {
        this.saveToFile_Chrome( "readme", 
        `
            DoubleClick         :   블럭추가/이미지 넣기
            Ctrl + click        :   다중선택
            Ctrl + z            :   실행취소(Undo)
            Ctrl + y            :   다시실행(Redo)
            Ctrl + d            :   복제
            Drag                :   Moving/Resize
            Shift + Drag        :   Snap
            Effect              :   블럭 클릭 후 Effect 탭에서 이미지 편집
            Animation Moving    : 
        `
        )
    }

    render() {
        const {handleMouseDown, readMe} = this;
        const {scale, tap, block, time,actives, timelineState, onChangeBlock, changeSaveFlag} = this.props;

        switch(tap){
            case '0':
                return(
                    <div className='menu'>
                        <button
                            className='big-btn'
                            onClick={this.export}
                        >Project Export</button>
                        <input
                            type='file'
                            className='d-none'
                            id='import'
                            accept='text/plain'
                            onInput={this.handleInput}
                        />
                         <label
                            className='big-btn'
                            htmlFor='import'
                            style={{marginBottom:'20px'}}
                        >Project Import</label>
                        <button className='big-btn' onClick={readMe}>Help</button>
                    </div>
                )


            case '1':
                if(block === undefined){
                    return( 
                            <div className='menu'>
                                -
                            </div>
                        )
                }
                return(
                    <div className='menu'>
                        <div className='block-name-wrapper'>
                            <span className='header-text'>Name :</span> 
                            <input className='block-name-input' value={block.name} readOnly></input>
                        </div>

                        <div className='block-info-wrapper'>
                            <div>
                                <span className='header-text'>Duration :</span> 
                                <div>
                                    <span className='edit-text' onMouseDown={handleMouseDown}  data-name='start'>{this.getTime(block.start)}</span>
                                    &nbsp;~ &nbsp;
                                    <span className='edit-text' onMouseDown={handleMouseDown}  data-name='end'>{this.getTime(block.end)}</span>
                                </div>
                            </div>
                            <div>
                                <span className='header-text'>Layer :</span> 
                                <span>{block.layer}</span>
                            </div>
                        </div>

                        
                    </div>
                )

            case '2':
                if(block === undefined){
                    return( 
                            <div className='menu'>
                                -
                            </div>
                        )
                }
                return(
                    <MenuEffect 
                        block = {block}
                        time = {time}
                        scale = {scale}
                        timelineState = {timelineState}
                        actives = {actives}


                        onChangeBlock = {onChangeBlock} 
                        changeSaveFlag = {changeSaveFlag}
                    />
                )

            default:
                return(
                    <div className='err-msg'>
                        err
                    </div>
                )
        }

    }
}

export default Menu